available_rooms = 70

elasticity = {
    70: 40.0,
    80: 34.0,
    90: 30.0,
    100: 27.0,
    110: 24.0,
    120: 22.0,
    140: 18.0,
    160: 15.0
}
expected_profit_day1 = []
expected_profit_day2 = []
profit_total = []
demand_1 = 0
demand_2 = 0
price_1 = 0

for price in elasticity:

    expected_profit_day1.append(elasticity[price]*price)

    max_day1 = max(expected_profit_day1)

    if max_day1 / price == elasticity[price]:
        price_1 = price
        demand_1 = elasticity[price]

print('The optimum price for the first day:',price_1,'€', '\nExpected demand:',demand_1)

available_rooms -= demand_1
print('Available rooms after the first reservation day:',available_rooms)

for price in elasticity:
    if elasticity[price] > available_rooms:
        elasticity[price] = 30

    expected_profit_day2.append(elasticity[price]*price)

    max_day2 = max(expected_profit_day2)

    if max_day2 / price == elasticity[price]:
        price_2 = price
        demand_2 = elasticity[price]

print('The optimum price for the second day:',price_2,'€', '\nExpected demand:',demand_2)
print('Total amount expected to make:', max_day1+max_day2,'€')
